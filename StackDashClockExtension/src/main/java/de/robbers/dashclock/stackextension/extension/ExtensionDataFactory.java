package de.robbers.dashclock.stackextension.extension;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.util.LongSparseArray;
import com.google.android.apps.dashclock.api.DashClockExtension;
import com.google.android.apps.dashclock.api.ExtensionData;
import de.robbers.dashclock.stackextension.R;
import de.robbers.dashclock.stackextension.Requests;
import de.robbers.dashclock.stackextension.Sites;
import de.robbers.dashclock.stackextension.model.AssociatedAccount;
import de.robbers.dashclock.stackextension.settings.Settings;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ExtensionDataFactory {
    private static final String TAG = ExtensionDataFactory.class.getSimpleName();

    private Context mContext;

    public ExtensionDataFactory(Context context) {
        mContext = context;
    }

    public ExtensionData fromReputationResponse(
            String reputationResponse, int reputation, String userId, Sites sites, String site) {
        if (reputationResponse == null) {
            return null;
        }

        int status = reputation;
        String expandedTitle = "";
        String expandedBody = "";

        int displayMode = Settings.getDisplayMode(mContext);
        LongSparseArray reputationArray = new LongSparseArray();
        try {
            JSONArray items = new JSONObject(reputationResponse).getJSONArray("items");
            Log.i(TAG, items.toString(2));

            for (int i = 0; i < items.length(); i++) {
                JSONObject item = items.getJSONObject(i);
                long postId = item.optLong("post_id");
                int reputationChange = item.optInt("reputation_change");
                if (displayMode == ReputationExtension.DISPLAY_NEW_REP) {
                    status += reputationChange;
                }
                int newValue = reputationChange;
                newValue += (Integer) reputationArray.get(postId, 0);
                reputationArray.put(postId, newValue);
            }
            List<Long> postIds = new ArrayList<Long>();
            for (int i = 0; i < items.length(); i++) {
                JSONObject item = items.getJSONObject(i);
                long postId = item.optLong("post_id");
                int reputationChange = item.optInt("reputation_change");
                if (postIds.contains(postId) || reputationChange == 0) {
                    continue;
                }
                postIds.add(postId);
                if (i < 2) {
                    int reputationValue = (Integer) reputationArray.get(postId);
                    String title = String.valueOf(Html.fromHtml(item.optString("title")));
                    expandedBody +=
                            buildExpandedBodyReputationPost(reputationValue, title, postIds.size());
                }
            }

            if (TextUtils.isEmpty(expandedBody)) {
                expandedBody = mContext.getString(R.string.no_recent_reputation_changes);
            }

            String statusText = NumberFormat.getNumberInstance(Locale.US).format(status);
            expandedTitle = statusText + " Reputation" + " \u2014 "
                    + sites.getNameFromApiParameter(site);
            String link = sites.getUrlFromApiParameter(site) + "/users/" + userId +
                    "?tab=reputation";
            int icon = sites.getIcon(site);

            return new ExtensionData()
                    .visible(status != 0)
                    .icon(icon)
                    .status(statusText)
                    .expandedTitle(expandedTitle)
                    .expandedBody(expandedBody)
                    .clickIntent(new Intent(Intent.ACTION_VIEW, Uri.parse(link)));

        } catch (JSONException e) {
            Log.i(TAG, reputationResponse);
            e.printStackTrace();
        }
        return null;
    }

    public ExtensionData fromReputationHistoryResponse(
            String response, int reputation, String userId, Sites sites, String site) {
        if (response == null) {
            return null;
        }

        int status = reputation;
        String expandedTitle = "";
        String expandedBody = "";

        int displayMode = Settings.getDisplayMode(mContext);
        LongSparseArray reputationArray = new LongSparseArray();
        try {
            JSONArray items = new JSONObject(response).getJSONArray("items");
            Log.i(TAG, items.toString(2));

            for (int i = 0; i < items.length(); i++) {
                JSONObject item = items.getJSONObject(i);
                long postId = item.optLong("post_id");
                int reputationChange = item.optInt("reputation_change");
                if (displayMode == ReputationExtension.DISPLAY_NEW_REP) {
                    status += reputationChange;
                }
                int newValue = reputationChange;
                newValue += (Integer) reputationArray.get(postId, 0);
                reputationArray.put(postId, newValue);
            }

            List<Long> postIds = new ArrayList<Long>();

            Requests requests = new Requests(mContext);

            for (int i = 0; i < items.length(); i++) {
                JSONObject item = items.getJSONObject(i);
                long postId = item.optLong("post_id");
                int reputationChange = item.optInt("reputation_change");
                if (postIds.contains(postId) || reputationChange == 0) {
                    continue;
                }
                postIds.add(postId);
                int reputationValue = (Integer) reputationArray.get(postId);
                if (i < 2) {
                    String answerResponse = requests.getAnswer(postId, site);
                    String title = getPostTitle(answerResponse);
                    if (title == null) {
                        String questionResponse = requests.getQuestion(postId, site);
                        title = getPostTitle(questionResponse);
                    }
                    expandedBody +=
                            buildExpandedBodyReputationPost(reputationValue,
                                    Html.fromHtml(title).toString(), postIds.size());
                }
            }

            if (TextUtils.isEmpty(expandedBody)) {
                expandedBody = mContext.getString(R.string.no_recent_reputation_changes);
            }

            String statusText = NumberFormat.getNumberInstance(Locale.US).format(status);
            expandedTitle = statusText + " Reputation" + " \u2014 "
                    + sites.getNameFromApiParameter(site);
            String link = sites.getUrlFromApiParameter(site) + "/users/" + userId +
                    "?tab=reputation";
            int icon = sites.getIcon(site);

            return new ExtensionData()
                    .visible(status != 0)
                    .icon(icon)
                    .status(statusText)
                    .expandedTitle(expandedTitle)
                    .expandedBody(expandedBody)
                    .clickIntent(new Intent(Intent.ACTION_VIEW, Uri.parse(link)));

        } catch (JSONException e) {
            Log.i(TAG, response);
            e.printStackTrace();
        }
        return null;
    }

    private String getPostTitle(String response) {
        try {
            JSONArray items = new JSONObject(response).optJSONArray("items");
            Log.i(TAG, items.toString(2));
            if (items.length() > 0) {
                JSONObject item = items.optJSONObject(0);
                return item.optString("title");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String buildExpandedBodyReputationPost(int reputationChange, String title, int posts) {
        String post = posts > 1 ? "\n" : "";
        post += reputationChange > 0 ? "+" + reputationChange
                : reputationChange;
        post += " \u2014 " + title;
        return post;
    }

    public ExtensionData fromInboxResponse(String response) {
        if (response == null) {
            return null;
        }

        try {
            JSONArray items = new JSONObject(response).getJSONArray("items");
            Log.i(TAG, items.toString(2));
            int status = items.length();
            String expandedTitle = status + " new ";
            expandedTitle += status > 1 ? "items" : "item";
            expandedTitle += " in your Stack Exchange inbox";
            int icon = R.drawable.ic_inbox;
            String expandedBody = null;
            Intent intent = null;
            if (status > 0) {
                JSONObject newest = items.optJSONObject(0);
                String link = newest.optString("link");
                String title = String.valueOf(Html.fromHtml(newest.optString("title")));
                String type = newest.optString("item_type");
                String body = String.valueOf(Html.fromHtml(newest.optString("body")));
                expandedBody = buildExpandedBodyInbox(type, title, body);
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
            }

            return new ExtensionData()
                    .visible(status > 0)
                    .icon(icon)
                    .status("" + status)
                    .expandedTitle(expandedTitle)
                    .expandedBody(expandedBody)
                    .clickIntent(intent);
        } catch (JSONException e) {
            Log.i(TAG, response);
            e.printStackTrace();
        }

        return null;
    }

    private String buildExpandedBodyInbox(String type, String title, String body) {
        String expandedBody = "New " + type.replace("new_", "").replace("_", " ");
        if (!TextUtils.isEmpty(title)) {
            expandedBody += " on " + title;
        }
        if (!TextUtils.isEmpty(body)) {
            expandedBody += ":\n";
            expandedBody += "\"" + body + "\"";
        }
        return expandedBody;
    }

    public ExtensionData fromNotificationsResponse(
            String response, List<AssociatedAccount> associatedAccounts) {
        if (response == null) {
            return null;
        }

        try {
            JSONArray items = new JSONObject(response).getJSONArray("items");
            Log.i(TAG, items.toString(2));
            int status = items.length();
            String expandedTitle = status + " new Stack Exchange ";
            expandedTitle += status > 1 ? "notifications" : "notification";
            int icon = R.drawable.ic_notifications;
            String expandedBody = null;
            Intent intent = null;
            if (status > 0) {
                String notifications[] = new String[items.length()];
                for (int i = 0; i < items.length(); i++) {
                    JSONObject item = items.optJSONObject(i);
                    notifications[i] = String.valueOf(Html.fromHtml(item.optString("body")));
                    if (i == 0) {
                        intent = createIntentForNotificationItem(item, associatedAccounts);
                    }
                }
                expandedBody = buildExpandedBodyNotifications(notifications);
            }
            return new ExtensionData()
                    .visible(status > 0)
                    .icon(icon)
                    .status("" + status)
                    .expandedTitle(expandedTitle)
                    .expandedBody(expandedBody)
                    .clickIntent(intent);
        } catch (JSONException e) {
            Log.i(TAG, response);
            e.printStackTrace();
        }
        return null;
    }

    private Intent createIntentForNotificationItem(
            JSONObject item, List<AssociatedAccount> associatedAccounts) {
        String siteUrl = item.optJSONObject("site").optString("site_url");
        String profileUrl = AssociatedAccount.
                getProfileUrlFromSiteUrl(associatedAccounts, siteUrl);
        String body = item.optString("body");
        String type = item.optString("notification_type");

        // default link is profile URL
        String link = profileUrl;

        if (type.equals("badge_earned")) {
            link += "?tab=badges&sort=recent";
        } else if (type == "reputation_bonus") {
            link += profileUrl + "?tab=reputation";
        } else {
            // if the body contains an URL, that is the link
            String[] split = body.split("\"");
            URL url = null;
            for (String s : split) {
                if (s.startsWith("http://") || s.startsWith("https://"))
                    try {
                        url = new URL(s);
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
            }
            if (url != null) {
                link = url.toString();
            }
        }

        return new Intent(Intent.ACTION_VIEW, Uri.parse(link));
    }

    private String buildExpandedBodyNotifications(String[] notifications) {
        String expandedBody = "";
        for (String notification : notifications) {
            expandedBody += TextUtils.isEmpty(expandedBody) ? "" : "\n";
            expandedBody += notification;
        }
        return expandedBody;
    }

    public ExtensionData getErrorUpdate(DashClockExtension extension, int code) {
        int stringResource = R.string.error_unknown;
        switch (code) {
            case ReputationExtension.ERROR_USER_SITE_COMBINATION:
                stringResource = R.string.error_user_site_combination;
                break;
            case Requests.ERROR_ACCESS_TOKEN:
                stringResource = R.string.error_missing_access_token;
                break;
            default:
                break;
        }

        String expandedTitle =
                extension instanceof ReputationExtension ?
                        mContext.getString(R.string.reputation_extension_title) :
                        mContext.getString(R.string.inbox_notification_extension_title);

        return new ExtensionData()
                .visible(true)
                .icon(R.drawable.ic_stackexchange)
                .status(mContext.getString(R.string.status_none))
                .expandedTitle(expandedTitle)
                .expandedBody(mContext.getString(stringResource));
    }

    public ExtensionData getEmptyUpdate() {
        return new ExtensionData()
                .visible(false)
                .icon(R.drawable.ic_stackexchange)
                .status("")
                .expandedTitle("")
                .expandedBody("");
    }
}
