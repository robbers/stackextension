package de.robbers.dashclock.stackextension.model;

import android.text.Html;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.robbers.dashclock.stackextension.Sites;

public class AssociatedAccount {
    private static final String TAG = AssociatedAccount.class.getSimpleName();

    private String mSiteName;
    private String mSiteUrl;
    private String mUserId;
    private long mReputation;

    public String getSiteName() {
        return mSiteName;
    }

    public void setSiteName(String siteName) {
        mSiteName = siteName;
    }

    public String getSiteUrl() {
        return mSiteUrl;
    }

    public void setSiteUrl(String siteUrl) {
        mSiteUrl = siteUrl;
    }

    public String getUserId() {
        return mUserId;
    }

    public void setUserId(String userId) {
        mUserId = userId;
    }

    public long getReputation() {
        return mReputation;
    }

    public void setReputation(long reputation) {
        mReputation = reputation;
    }

    public static List<AssociatedAccount> getListFromResponse(String response) {
        if (response == null) {
            return null;
        }

        try {
            JSONObject associated = new JSONObject(response);
            JSONArray items = associated.optJSONArray("items");
            Log.i(TAG, items.toString(2));
            ArrayList<AssociatedAccount> associatedAccounts = new ArrayList<AssociatedAccount>();
            for (int i = 0; i < items.length(); i++) {
                JSONObject item = items.optJSONObject(i);
                AssociatedAccount aa = new AssociatedAccount();
                aa.setSiteName(item.optString("site_name"));
                aa.setSiteUrl(item.optString("site_url"));
                aa.setUserId(item.optString("user_id"));
                aa.setReputation(item.optLong("reputation"));
                associatedAccounts.add(aa);
            }
            return associatedAccounts;
        } catch (JSONException e) {
            Log.e(TAG, response);
            e.printStackTrace();
        }
        return null;
    }

    public static CharSequence[] getSiteNamesFromList(List<AssociatedAccount> list) {
        CharSequence[] siteNames = new CharSequence[list.size()];
        for (int i = 0; i < list.size(); i++) {
            AssociatedAccount account = list.get(i);
            siteNames[i] = Html.fromHtml(account.getSiteName());
        }
        return siteNames;
    }

    public static CharSequence[] getApiParametersFromList(List<AssociatedAccount> list, Sites sites) {
        CharSequence[] apiParameters = new CharSequence[list.size()];
        for (int i = 0; i < list.size(); i++) {
            AssociatedAccount account = list.get(i);
            apiParameters[i] = sites.getApiParameterFromUrl(account.getSiteUrl());
        }
        return apiParameters;
    }

    public static String getProfileUrlFromSiteUrl(List<AssociatedAccount> list, String siteUrl) {
        for (AssociatedAccount aa : list) {
            if (aa.getSiteUrl().equals(siteUrl)) {
                return aa.getSiteUrl() + "/users/" + aa.getUserId();
            }
        }
        return null;
    }
}
