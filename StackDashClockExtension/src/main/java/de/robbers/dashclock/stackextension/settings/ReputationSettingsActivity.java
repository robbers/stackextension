package de.robbers.dashclock.stackextension.settings;

import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.text.TextUtils;

import java.util.List;

import de.robbers.dashclock.stackextension.R;
import de.robbers.dashclock.stackextension.Sites;
import de.robbers.dashclock.stackextension.Store;
import de.robbers.dashclock.stackextension.model.AssociatedAccount;

public class ReputationSettingsActivity extends SettingsActivity {

    private ListPreference mPrefSite;
    private ListPreference mPrefDisplay;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupSimplePreferencesScreen();
    }

    private void setupSimplePreferencesScreen() {
        addPreferencesFromResource(R.xml.pref_reputation);

        mPrefSite = (ListPreference) findPreference(Settings.PREF_SITE);
        if (TextUtils.isEmpty(Settings.getSite(this))) {
            Settings.putSite(this, getString(R.string.pref_site_default));
        }

        mPrefDisplay = (ListPreference) findPreference(Settings.PREF_DISPLAY);
        mPrefUpdateWhenScreenOn = (CheckBoxPreference)
                findPreference(Settings.PREF_UPDATE_WHEN_SCREEN_ON_REPUTATION);

        bindPreferenceSummaryToValue(mPrefSite);
        bindPreferenceSummaryToValue(mPrefDisplay);
    }

    @Override
    protected void updatePreferences() {
        super.updatePreferences();

        mPrefSite.setEnabled(mAccessToken != null);
        if (mAccessToken != null) {
            List<AssociatedAccount> associated = Store.getAssociated(this);
            Sites sites = new Sites(this);
            mPrefSite.setEntries(AssociatedAccount.getSiteNamesFromList(associated));
            mPrefSite.setEntryValues(AssociatedAccount.getApiParametersFromList(associated, sites));
            bindPreferenceSummaryToValue(mPrefSite);
        }

        mPrefDisplay.setEnabled(mAccessToken != null);
    }
}
