package de.robbers.dashclock.stackextension;

import android.content.Context;

import de.robbers.dashclock.stackextension.extension.ReputationExtension;
import de.robbers.dashclock.stackextension.util.CalendarUtils;

public class RequestsCompat extends Requests {
    private static final String TAG = RequestsCompat.class.getSimpleName();

    public RequestsCompat(Context context) {
        super(context);
    }

    public String getUser(String userId, String site) {
        String url = "http://api.stackexchange.com/2.1/users/" + userId
                + "?filter=!G*klMsSp-SWXyRjqiNhAb-lDDP&site=" + site;
        return get(url);
    }

    public String getReputation(String userId, String site, int displayMode) {
        long from = CalendarUtils.getToday() / 1000;
        if (displayMode == ReputationExtension.DISPLAY_TOTAL_REP) {
            from = CalendarUtils.getOneWeekAgo() / 1000;
        }

        long to = CalendarUtils.getTomorrow() / 1000;

        String url = "http://api.stackexchange.com/2.1/users/" + userId
                + "/reputation?fromdate=" + from + "&todate=" + to
                + "&filter=!A6zx8gZ1_N(X9" + "&site=" + site;
        return get(url);
    }
}
