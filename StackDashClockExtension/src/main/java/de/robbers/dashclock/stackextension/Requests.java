package de.robbers.dashclock.stackextension;

import android.content.Context;
import android.util.Log;

import com.squareup.okhttp.OkHttpClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import de.robbers.dashclock.stackextension.extension.ReputationExtension;
import de.robbers.dashclock.stackextension.util.CalendarUtils;

public class Requests {
    public static final int ERROR_ACCESS_TOKEN = 401;

    private static final String TAG = Requests.class.getSimpleName();
    private Context mContext;
    private OkHttpClient mClient;
    private String mAccessToken;
    private int mErrorId;

    public Requests(Context context) {
        mContext = context;
        mClient = new OkHttpClient();
        mAccessToken = Store.getAccessToken(context);
    }

    protected String get(String url) {
        Log.i(TAG, url);
        InputStream in = null;
        try {
            HttpURLConnection connection = mClient.open(new URL(url));
            if (connection.getResponseCode() != 200) {
                in = connection.getErrorStream();
            } else {
                in = connection.getInputStream();
            }
            String response = new String(readFully(in), "UTF-8");
            if (connection.getResponseCode() != 200) {
                extractErrorId(response);
                if (mErrorId == ERROR_ACCESS_TOKEN) {
                    Store.putAccessToken(mContext, null);
                }
                return null;
            } else {
                return response;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (in != null) try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private byte[] readFully(InputStream in) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        for (int count; (count = in.read(buffer)) != -1; ) {
            out.write(buffer, 0, count);
        }
        return out.toByteArray();
    }

    public String getUser(String site) {
        String url = "https://api.stackexchange.com/2.1/me?key=kSi41xSbAMIOuHs3UdBbxA((" +
                "&access_token=" + mAccessToken + "&site=" + site +
                "&filter=!G*klMsSp-SWXyRjqiNhAb-lDDP";
        return get(url);
    }

    public String getReputation(String site, long lastAccessDate, int displayMode) {
        long from = CalendarUtils.getToday() / 1000;

        if (displayMode == ReputationExtension.DISPLAY_NEW_REP) {
            from = lastAccessDate;
        }

        if (displayMode == ReputationExtension.DISPLAY_TOTAL_REP) {
            from = CalendarUtils.getOneWeekAgo() / 1000;
        }

        long to = CalendarUtils.getTomorrow() / 1000;

        String url = "https://api.stackexchange.com/me/reputation?" +
                "key=kSi41xSbAMIOuHs3UdBbxA((&access_token=" + mAccessToken + "&fromdate="
                + from + "&todate=" + to + "&site=" + site + "&filter=!A6zx8gZ1_N(X9";
        return get(url);
    }

    public String getReputationHistory(String site, long lastAccessDate, int displayMode) {
        long from = CalendarUtils.getToday() / 1000;

        if (displayMode == ReputationExtension.DISPLAY_NEW_REP) {
            from = lastAccessDate;
        }

        if (displayMode == ReputationExtension.DISPLAY_TOTAL_REP) {
            from = CalendarUtils.getOneWeekAgo() / 1000;
        }

        long to = CalendarUtils.getTomorrow() / 1000;

        String url = "https://api.stackexchange.com/me/reputation-history/full?" +
                "key=kSi41xSbAMIOuHs3UdBbxA((&access_token=" + mAccessToken + "&fromdate="
                + from + "&todate=" + to + "&site=" + site +"&filter=!4-_EI.k3qC5vfw.oX";
        return get(url);
    }

    public String getAnswer(long id, String site) {
        String url = "https://api.stackexchange.com/answers/"+ id +"?site=" + site
                +"&filter=!)R0EWdp6djE.Eg*g6Xxhi*Bu";
        return get(url);
    }

    public String getQuestion(long id, String site) {
        String url = "https://api.stackexchange.com/questions/"+ id +"?site=" + site
                +"&filter=!-MBjrRpSPefnUfjWtETNMEy_0oglsledi";
        return get(url);
    }

    public String getAssociated() {
        String url = "https://api.stackexchange.com/me/associated?" +
                "key=kSi41xSbAMIOuHs3UdBbxA((&access_token=" + mAccessToken +
                "&pagesize=100&filter=!SmBjDpYAn*mfgYX2ae";
        return get(url);
    }

    public String getInbox() {
        String url = "https://api.stackexchange.com/inbox/unread?" +
                "key=kSi41xSbAMIOuHs3UdBbxA((&access_token=" + mAccessToken +
                "&pagesize=100&filter=!0UYY1Gk4rN9.d(QJU9.FmB.rS";
        return get(url);
    }

    public String getNotifications() {
        String url = "https://api.stackexchange.com/notifications/unread?" +
                "key=kSi41xSbAMIOuHs3UdBbxA((&access_token=" + mAccessToken +
                "&pagesize=100&filter=!0UYp1wbuPRI6MAJSuoxLjkwIa";
        return get(url);
    }

    private void extractErrorId(String response) {
        try {
            JSONObject json = new JSONObject(response);
            mErrorId = json.optInt("error_id", 0);
            if (mErrorId > 0) {
                Log.e(TAG, json.toString(2));
                if (mErrorId == 401 || mErrorId == 402 || mErrorId == 403) {
                    mErrorId = ERROR_ACCESS_TOKEN;
                }
            }
        } catch (JSONException e) {
            Log.e(TAG, response);
            e.printStackTrace();
        }
    }
}
