package de.robbers.dashclock.stackextension.settings;

import android.os.Bundle;
import android.preference.CheckBoxPreference;

import de.robbers.dashclock.stackextension.R;

public class InboxNotificationsSettingsActivity extends SettingsActivity {
    private CheckBoxPreference mPrefShowInboxItems;
    private CheckBoxPreference mPrefShowNotifications;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupSimplePreferencesScreen();
    }

    private void setupSimplePreferencesScreen() {
        addPreferencesFromResource(R.xml.pref_inbox_notifications);

        mPrefShowInboxItems = (CheckBoxPreference) findPreference(Settings.PREF_SHOW_INBOX);
        mPrefShowNotifications = (CheckBoxPreference)
                findPreference(Settings.PREF_SHOW_NOTIFICATIONS);
        mPrefUpdateWhenScreenOn = (CheckBoxPreference)
                findPreference(Settings.PREF_UPDATE_WHEN_SCREEN_ON_INBOX_NOTIFICATIONS);
    }

    @Override
    protected void updatePreferences() {
        super.updatePreferences();
        mPrefShowInboxItems.setEnabled(mAccessToken != null);
        mPrefShowNotifications.setEnabled(mAccessToken != null);
    }
}
