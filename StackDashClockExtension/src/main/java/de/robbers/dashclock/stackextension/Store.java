package de.robbers.dashclock.stackextension;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.List;

import de.robbers.dashclock.stackextension.model.AssociatedAccount;

public class Store {
    private static String KEY_ACCESS_TOKEN = "access_token";
    private static String KEY_ASSOCIATED = "associated";

    public static void putAccessToken(Context context, String accessToken) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        if (accessToken == null) {
            editor.remove(KEY_ACCESS_TOKEN);
        } else {
            editor.putString(KEY_ACCESS_TOKEN, accessToken);
        }
        editor.commit();
    }

    public static String getAccessToken(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(KEY_ACCESS_TOKEN, null);
    }

    public static void putAssociated(Context context, String associatedResponse) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        if (associatedResponse == null) {
            editor.remove(KEY_ASSOCIATED);
        } else {
            editor.putString(KEY_ASSOCIATED, associatedResponse);
        }
        editor.commit();
    }

    public static List<AssociatedAccount> getAssociated(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String response = preferences.getString(KEY_ASSOCIATED, null);
        return AssociatedAccount.getListFromResponse(response);
    }
}
