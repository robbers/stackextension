package de.robbers.dashclock.stackextension.settings;

import android.app.Dialog;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.preference.RingtonePreference;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.util.List;

import de.robbers.dashclock.stackextension.R;
import de.robbers.dashclock.stackextension.Requests;
import de.robbers.dashclock.stackextension.Store;
import de.robbers.dashclock.stackextension.model.AssociatedAccount;
import de.robbers.dashclock.stackextension.util.Utils;

public class SettingsActivity extends PreferenceActivity {
    private static final String DIALOG_URL = "https://stackexchange.com/oauth/dialog?" +
            "client_id=2209&scope=no_expiry,read_inbox,private_info&" +
            "redirect_uri=https://stackexchange.com/oauth/login_success";
    protected String mAccessToken;
    protected Preference mPrefSignIn;
    protected CheckBoxPreference mPrefUpdateWhenScreenOn;

    private View mProgress;
    private Dialog mDialog;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_settings);

        if (!TextUtils.isEmpty(Settings.getUserId(this))) {
            showUpgradeScreen();
        }
    }

    private void showUpgradeScreen() {
        final View upgradeScreen = findViewById(R.id.upgrade_screen);
        upgradeScreen.setVisibility(View.VISIBLE);
        final View preferences = findViewById(android.R.id.list);
        preferences.setVisibility(View.GONE);

        findViewById(R.id.button_not_now).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        findViewById(R.id.button_upgrade).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Settings.removeUserId(SettingsActivity.this);
                upgradeScreen.setVisibility(View.GONE);
                preferences.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mPrefSignIn = findPreference(Settings.PREF_SIGN_IN);
        updatePreferences();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_about:
                Utils.showAboutDialog(this);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    protected void signInDialog() {
        mDialog = new Dialog(SettingsActivity.this);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        // Get the layout inflater
        final LayoutInflater inflater = getLayoutInflater();
        final View view = inflater.inflate(R.layout.dialog_signin, null);
        final WebView webView = (WebView) view.findViewById(R.id.webview);
        mProgress = view.findViewById(android.R.id.progress);
        webView.loadUrl(DIALOG_URL);
        webView.getSettings().setJavaScriptEnabled(true);

        mDialog.setContentView(view);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                if (url.contains("access_token=")) {
                    String accessToken = url.substring(
                            url.indexOf("access_token=") + "access_token=".length());
                    Store.putAccessToken(SettingsActivity.this, accessToken);
                    webView.setVisibility(View.INVISIBLE);
                    new GetAssociatedAccountsTask().execute();
                }
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                mProgress.setVisibility(View.GONE);
            }
        });

        mDialog.show();
    }

    protected void updatePreferences() {
        mAccessToken = Store.getAccessToken(this);
        if (mAccessToken != null) {
            mPrefSignIn.setTitle(R.string.sign_out);
            mPrefSignIn.setSummary(null);
            mPrefSignIn.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    Store.putAccessToken(SettingsActivity.this, null);
                    updatePreferences();
                    return false;
                }
            });
        } else {
            mPrefSignIn.setTitle(R.string.sign_in);
            mPrefSignIn.setSummary(R.string.tap_to_sign_in);
            mPrefSignIn.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    signInDialog();
                    return false;
                }
            });
        }
        mPrefUpdateWhenScreenOn.setEnabled(mAccessToken != null);
    }

    class GetAssociatedAccountsTask extends AsyncTask<Void, Void, List<AssociatedAccount>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgress.setVisibility(View.VISIBLE);
        }

        @Override
        protected List<AssociatedAccount> doInBackground(Void... params) {
            Requests requests = new Requests(SettingsActivity.this);
            String associatedResponse = requests.getAssociated();
            Store.putAssociated(SettingsActivity.this, associatedResponse);
            return AssociatedAccount.getListFromResponse(associatedResponse);
        }

        @Override
        protected void onPostExecute(List<AssociatedAccount> associatedAccounts) {
            super.onPostExecute(associatedAccounts);
            if (associatedAccounts != null) {
                updatePreferences();
                mDialog.dismiss();
            } else {
                Store.putAccessToken(SettingsActivity.this, null);
            }
        }
    }

    /**
     * A preference value change listener that updates the preference's summary
     * to reflect its new value.
     */
    private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object value) {
            String stringValue = value.toString();

            if (preference instanceof ListPreference) {
                // For list preferences, look up the correct display value in
                // the preference's 'entries' list.
                ListPreference listPreference = (ListPreference) preference;
                int index = listPreference.findIndexOfValue(stringValue);

                // Set the summary to reflect the new value.
                preference.setSummary(
                        index >= 0
                                ? listPreference.getEntries()[index]
                                : null);

            } else if (preference instanceof RingtonePreference) {
                // For ringtone preferences, look up the correct display value
                // using RingtoneManager.
                if (TextUtils.isEmpty(stringValue)) {
                    // Empty values correspond to 'silent' (no ringtone).
                    // preference.setSummary(R.string.pref_ringtone_silent);

                } else {
                    Ringtone ringtone = RingtoneManager.getRingtone(
                            preference.getContext(), Uri.parse(stringValue));

                    if (ringtone == null) {
                        // Clear the summary if there was a lookup error.
                        preference.setSummary(null);
                    } else {
                        // Set the summary to reflect the new ringtone display
                        // name.
                        String name = ringtone.getTitle(preference.getContext());
                        preference.setSummary(name);
                    }
                }

            } else {
                // For all other preferences, set the summary to the value's
                // simple string representation.
                preference.setSummary(stringValue);
            }
            return true;
        }
    };

    /**
     * Binds a preference's summary to its value. More specifically, when the
     * preference's value is changed, its summary (line of text below the
     * preference title) is updated to reflect the value. The summary is also
     * immediately updated upon calling this method. The exact display format is
     * dependent on the type of preference.
     * 
     * @see #sBindPreferenceSummaryToValueListener
     */
    protected static void bindPreferenceSummaryToValue(Preference preference) {
        // Set the listener to watch for value changes.
        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

        // Trigger the listener immediately with the preference's
        // current value.
        sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                PreferenceManager
                        .getDefaultSharedPreferences(preference.getContext())
                        .getString(preference.getKey(), ""));
    }
}
