package de.robbers.dashclock.stackextension.extension;

import android.text.TextUtils;
import android.util.Log;

import com.google.android.apps.dashclock.api.DashClockExtension;
import com.google.android.apps.dashclock.api.ExtensionData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.robbers.dashclock.stackextension.Requests;
import de.robbers.dashclock.stackextension.RequestsCompat;
import de.robbers.dashclock.stackextension.Sites;
import de.robbers.dashclock.stackextension.Store;
import de.robbers.dashclock.stackextension.settings.Settings;

public class ReputationExtension extends DashClockExtension {
    private static final String TAG = ReputationExtension.class.getSimpleName();

    public static final int DISPLAY_TOTAL_REP = 0;
    public static final int DISPLAY_TODAYS_REP = 1;
    public static final int DISPLAY_NEW_REP = 2;

    public static final int ERROR_USER_SITE_COMBINATION = 0;

    private ExtensionDataFactory mExtensionDataFactory;

    private Sites mSites;
    private String mUserId;
    private String mSite;
    private int mDisplayMode;
    private int mReputation;
    private long mLastAccessDate;

    @Override
    protected void onInitialize(boolean isReconnect) {
        super.onInitialize(isReconnect);
        mSites = new Sites(this);
    }

    @Override
    protected void onUpdateData(int reason) {
        mExtensionDataFactory = new ExtensionDataFactory(this);
        mSite = Settings.getSite(this);
        mReputation = 0;

        setUpdateWhenScreenOn(Settings.updateWhenScreenOn(this));

        if (TextUtils.isEmpty(mSite)) {
            Log.e(TAG, "Data missing");
            return;
        }

        mDisplayMode = Settings.getDisplayMode(this);

//        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
//        editor.putString(Settings.PREF_USER_ID, "1018177").commit();

        if (!TextUtils.isEmpty(Settings.getUserId(this))) {
            compat();
        } else {
            v2();
        }
    }

    private void v2() {
        String accessToken = Store.getAccessToken(this);
        if (!isAccessTokenValid(accessToken)) {
            return;
        }

        Requests requests = new Requests(this);

        String userResponse = requests.getUser(mSite);
        accessToken = Store.getAccessToken(this);
        if (!isAccessTokenValid(accessToken)) {
            return;
        }
        if (userResponse == null) {
            return;
        }
        parseUserResponse(userResponse);

        String reputationHistoryResponse =
                requests.getReputationHistory(mSite, mLastAccessDate, mDisplayMode);
        ExtensionData extensionData = mExtensionDataFactory.fromReputationHistoryResponse(
                reputationHistoryResponse, mReputation, mUserId, mSites, mSite);
        if (extensionData != null) {
            publishUpdate(extensionData);
        }
    }

    private void compat() {
        mUserId = Settings.getUserId(this);
        if (TextUtils.isEmpty(mUserId)) {
            Log.e(TAG, "Data missing");
            return;
        }
        RequestsCompat requestsCompat = new RequestsCompat(this);
        String userResponse = requestsCompat.getUser(mUserId, mSite);
        parseUserResponse(userResponse);

        String reputationResponse = requestsCompat.getReputation(mUserId, mSite, mDisplayMode);
        ExtensionData extensionData = mExtensionDataFactory.fromReputationResponse(
                reputationResponse, mReputation, mUserId, mSites, mSite);
        if (extensionData != null) {
            publishUpdate(extensionData);
        }
    }

    private void parseUserResponse(String response) {
        if (response == null) {
            return;
        }
        try {
            JSONArray items = new JSONObject(response).getJSONArray("items");
            Log.i(TAG, items.toString(2));
            if (items.length() == 0) {
                publishUpdate(mExtensionDataFactory.getErrorUpdate(this, ERROR_USER_SITE_COMBINATION));
                return;
            }
            JSONObject user = items.getJSONObject(0);
            mUserId = String.valueOf(user.getLong("user_id"));
            int displayMode = Settings.getDisplayMode(this);
            switch (displayMode) {
                case DISPLAY_TOTAL_REP:
                    mReputation = user.getInt("reputation");
                    break;
                case DISPLAY_TODAYS_REP:
                    mReputation = user.getInt("reputation_change_day");
                    break;
                case DISPLAY_NEW_REP:
                    mLastAccessDate = user.getLong("last_access_date");
                    break;
            }
        } catch (JSONException e) {
            Log.i(TAG, response);
            e.printStackTrace();
        }
    }

    private boolean isAccessTokenValid(String accessToken) {
        if (accessToken == null) {
            publishUpdate(
                    mExtensionDataFactory.getErrorUpdate(this, Requests.ERROR_ACCESS_TOKEN));
            return false;
        }
        return true;
    }
}
