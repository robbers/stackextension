package de.robbers.dashclock.stackextension.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.android.apps.dashclock.api.DashClockExtension;

import de.robbers.dashclock.stackextension.R;
import de.robbers.dashclock.stackextension.extension.ReputationExtension;

public class Settings {
    public static final String PREF_SIGN_IN = "pref_sign_in";
    public static final String PREF_SITE = "pref_site";
    public static final String PREF_USER_ID = "pref_user_id";
    public static final String PREF_DISPLAY = "pref_display";
    public static final String PREF_SHOW_INBOX = "pref_show_inbox";
    public static final String PREF_SHOW_NOTIFICATIONS = "pref_show_notifications";
    public static final String PREF_UPDATE_WHEN_SCREEN_ON_REPUTATION =
            "pref_update_when_screen_on_reputation";
    public static final String PREF_UPDATE_WHEN_SCREEN_ON_INBOX_NOTIFICATIONS =
            "pref_update_when_screen_on_inbox_notifications";

    public static String getSite(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(PREF_SITE, "");
    }

    public static void putSite(Context context, String site) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(PREF_SITE, site);
        editor.commit();
    }

    public static String getUserId(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(PREF_USER_ID, "");
    }

    public static void removeUserId(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.remove(PREF_USER_ID);
        editor.commit();
    }

    public static int getDisplayMode(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);

        String display = sp.getString(PREF_DISPLAY, null);

        if (display == null || display.equals(context.getString(R.string.display_total_rep))) {
            return ReputationExtension.DISPLAY_TOTAL_REP;
        } else if (display.equals(context.getString(R.string.display_todays_rep))) {
            return ReputationExtension.DISPLAY_TODAYS_REP;
        } else if (display.equals(context.getString(R.string.display_unread_rep))) {
            return ReputationExtension.DISPLAY_NEW_REP;
        }

        return ReputationExtension.DISPLAY_TOTAL_REP;
    }

    public static boolean showInbox(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getBoolean(PREF_SHOW_INBOX, true);
    }

    public static boolean showNotifications(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getBoolean(PREF_SHOW_NOTIFICATIONS, true);
    }

    public static boolean updateWhenScreenOn(DashClockExtension extension) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(extension);
        if (extension instanceof ReputationExtension) {
            return sp.getBoolean(PREF_UPDATE_WHEN_SCREEN_ON_REPUTATION, true);
        } else {
            return sp.getBoolean(PREF_UPDATE_WHEN_SCREEN_ON_INBOX_NOTIFICATIONS, true);
        }
    }
}
