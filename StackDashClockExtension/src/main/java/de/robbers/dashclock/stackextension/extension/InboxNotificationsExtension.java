package de.robbers.dashclock.stackextension.extension;

import com.google.android.apps.dashclock.api.DashClockExtension;
import com.google.android.apps.dashclock.api.ExtensionData;

import java.util.List;

import de.robbers.dashclock.stackextension.Requests;
import de.robbers.dashclock.stackextension.Store;
import de.robbers.dashclock.stackextension.model.AssociatedAccount;
import de.robbers.dashclock.stackextension.settings.Settings;

public class InboxNotificationsExtension extends DashClockExtension {
    private static final String TAG = InboxNotificationsExtension.class.getSimpleName();

    private ExtensionDataFactory mExtensionDataFactory;

    @Override
    protected void onInitialize(boolean isReconnect) {
        super.onInitialize(isReconnect);
    }

    @Override
    protected void onUpdateData(int reason) {
        setUpdateWhenScreenOn(Settings.updateWhenScreenOn(this));

        mExtensionDataFactory = new ExtensionDataFactory(this);
        ExtensionData extensionData = null;

        String accessToken = Store.getAccessToken(this);
        if (!isAccessTokenValid(accessToken)) {
            return;
        }

        Requests requests = new Requests(this);

        List<AssociatedAccount> associatedAccounts = Store.getAssociated(this);

        boolean showInbox = Settings.showInbox(this);
        boolean showNotifications = Settings.showNotifications(this);

        if (!showInbox && !showNotifications) {
            extensionData = mExtensionDataFactory.getEmptyUpdate();
        }

        // Get inbox
        if (showInbox) {
            String inboxResponse = requests.getInbox();
            extensionData = mExtensionDataFactory.fromInboxResponse(inboxResponse);
        }

        // Get notifications
        if (showNotifications && (extensionData == null || !extensionData.visible())) {
            accessToken = Store.getAccessToken(this);
            if (!isAccessTokenValid(accessToken)) {
                return;
            }
            String notificationsResponse = requests.getNotifications();
            extensionData = mExtensionDataFactory.fromNotificationsResponse(
                    notificationsResponse, associatedAccounts);
        }

        if (extensionData != null) {
            publishUpdate(extensionData);
        }
    }

    private boolean isAccessTokenValid(String accessToken) {
        if (accessToken == null) {
            publishUpdate(
                    mExtensionDataFactory.getErrorUpdate(this, Requests.ERROR_ACCESS_TOKEN));
            return false;
        }
        return true;
    }
}
